---
title: Permissions
sidebar_label: Permissions
---

import APITable from '/src/components/APITable';

These are the permissions for the plugin. All commands in the commands column are separated by commas.

**NOTE:** All permissions must be given on the proxy


### Free and premium
These permission are for the free and premium versions

<APITable>

| Permission                        | Description                                                                                                                                                     | Command(s)      |
|-----------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|
| `ajqueue.joinfull`                | The plugin will still attempt to send you to full servers. Requires another plugin on the target server that will let you join when its full (e.g. Essentials). |                 |
| `ajqueue.joinfullserver.<server>` | Same as the `ajqueue.joinfull` permission, but only for `<server>`                                                                                              |                 |
| `ajqueue.manage.reload`           | Allows you to reload the config.                                                                                                                                | /ajqueue reload |
| `ajqueue.manage.update`           | Allows you to reload the config.                                                                                                                                | /ajqueue update |
| `ajqueue.manage.pause`            | Allows you to pause servers.                                                                                                                                    | /ajqueue pause  |
| `ajqueue.manage.send`             | Allows you to send people to queues.                                                                                                                            | /ajqueue send   |
| `ajqueue.manage.kick`             | Allows you to kick players from queues.                                                                                                                         | /ajqueue kick   |
| `ajqueue.manage.list`             | Allows you to list the queues and the people in them                                                                                                            | /ajqueue list   |
| `ajqueue.bypasspaused`            | Allows you to bypass paused queues. Must be enabled in the config.                                                                                              |
| `ajqueue.listqueues`              | Allows you to list the queues (more user-friendly command than /ajqueue list)                                                                                   | /listqueues     |

</APITable>

### Free only
These permissions are only for the free version. They have alternatives in the premium version, see below.


<APITable>

| Permission                         | Description                                                                                     | Command(s) |
|------------------------------------|-------------------------------------------------------------------------------------------------|------------|
| `ajqueue.priority`                 | Gives the user priority, which will put them infront of players without priority                |
| `ajqueue.serverpriority.<sevrver>` | Gives the user priority for `<server>`, which will put them infront of players without priority |
| `ajqueue.stayqueued`               | Allows the player to stay queued 60 seconds after they disconnect from the proxy.               |

</APITable>

### Premium only
These permission are for ajQueuePlus only.

<APITable>

| Permission                                 | Description                                                                                                                                               | Command(s) |
|--------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|------------|
| `ajqueue.bypass`                           | Bypasses the queue and attempts to send you immediately.  If you are not able to join, you will be put in the first position in the queue.                |
| `ajqueue.bypassserver.<server>`            | Bypasses the queue for `<server>` and attempts to send you immediately.  If you are not able to join, you will be put in the first position in the queue. |
| `ajqueue.priority.<number>`                | Gives the user priority, which will put them infront of players with lower priority (all players get a priority of 0 by default)                          |
| `ajqueue.serverpriority.<server>.<number>` | Gives the user priority for `<server>`, which will put them infront of players with lower priority (all players get a priority of 0 by default)           |
| `ajqueue.joinfullandbypass`                | Combines the functionality of the bypass permission and the fulljoin permission                                                                           |
| `ajqueue.joinfullandbypass.<server>`       | Combines the functionality of the bypassserver permission and the fulljoinserver permission                                                               |
| `ajqueue.stayqueued.<seconds>`             | Allows the player to stay queued `<seconds>` seconds after they disconnect from the proxy.                                                                |

</APITable>