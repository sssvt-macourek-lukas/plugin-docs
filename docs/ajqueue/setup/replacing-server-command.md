---
title: Replacing server command
sidebar_label: Replacing server command
---
It is fairly simple to replace the proxy's default /server command with the queue command.

## Replace the server command with the queue command

### Bungeecord
1. Open modules.yml (in the bungeecord server folder)
2. Remove the line that says `'jenkins://cmd_server'`
3. Save and close modules.yml
4. Delete cmd_server.jar in the modules folder
5. Set `enable-server-command` to true in ajQueue's config
6. Restart bungeecord

### Velocity
1. Set `enable-server-command` to true in ajQueue's config
2. Restart velocity


## Undo replacing the server command

### Bungeecord
Follow these steps to undo this:
1. Delete modules.yml (or just add back the line that says `'jenkins://cmd_server'`)
2. Set `enable-server-command` to false in ajQueue's config
3. Restart bungeecord

### Velocity
1. Set `enable-server-command` to false in ajQueue's config
2. Restart velocity
