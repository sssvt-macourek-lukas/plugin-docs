---
title: Spigot config
sidebar_label: Spigot config
---
import ConfigFetch from '/src/components/ConfigFetch';

This is the config for the spigot-side. There is only one option.

You **do not** need to copy the bungeecord config here. Anything in the bungee config that would effect the spigot server is taken from the bungee config automatically.


<ConfigFetch url="https://raw.githubusercontent.com/ajgeiss0702/ajQueue/master/spigot/src/main/resources/spigot-config.yml"></ConfigFetch>
