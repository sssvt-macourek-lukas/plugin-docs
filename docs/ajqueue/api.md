---
id: api
title: API
sidebar_label: API
---
You are able to hook into ajQueue and add people to the queue directly, list players in queues, etc.

Note that to follow this, you need to be running ajQueue 2.0.4 or newer.

If you need any help with the api, feel free to ask me for help. I prefer discord, but any way to contact me should be fine.

Note, the API can only be used on the proxy side.

## Depending on ajQueue
<details><summary>Maven</summary>

```xml
<repositories>
  <repository>
    <id>ajRepo</id>
    <url>https://repo.ajg0702.us/releases</url>
  </repository>
</repositories>

<dependencies>
  <dependency>
    <groupId>us.ajg0702.queue.api</groupId>
    <artifactId>api</artifactId>
    <version>{ajqueue version}</version>
    <scope>provided</scope>
  </dependency>
</dependencies>
```

</details>

<details><summary>Gradle (Kotlin)</summary>

```kotlin
repositories {
    maven { url = uri("https://repo.ajg0702.us/releases") }
}

dependencies {
    compileOnly("us.ajg0702.queue.api:api:{ajqueue version}")
}
```

</details>


<details><summary>Gradle (Groovy)</summary>

```groovy
repositories {
    maven { url 'https://repo.ajg0702.us/releases' }
}

dependencies {
    compileOnly 'us.ajg0702.queue.api:api:{ajqueue version}'
}
```

</details>



Make sure to replace `{ajqueue version}` with the latest version of ajQueue.

## Accessing the API

Once you have the dependency in your ide, you can start using the api.

To get an instance of the api, you can use this:

```java
AjQueueAPI.getInstance()
```

You can find the [javadocs for the api class here](https://ajg0702.gitlab.io/ajqueue/us/ajg0702/queue/api/AjQueueAPI.html).

Most of what you would want to do would be under `getQueueManager()`.

Remember, you can only access the API on the proxy side.

## Getting AdaptedPlayer

To get an AdaptedPlayer (which is used in most of ajQueue's api), you use the PlatformMethods class.

You can either use `PlatformMethods#getPlayer(String name)` or `PlatformMethods#getPlayer(UUID uuid)`.

Example:
```java
AdaptedPlayer player = AjQueueAPI.getInstance().getPlatformMethods().getPlayer("ajgeiss0702");
```

Note that the player has to be online.
