---
id: time-deprecation
title: Time Placeholder Deprecation
---
All the time placeholders (you can tell if a placeholder is a time placeholder if it says `time` in it - usually at the end)
that are in ajLeaderboards (`ajlb_...` placeholders) are being deprecated, and will be removed in a future release.

## Why?

The time placeholders are simply not needed anymore. As of ajLeaderboards v2.6.4, there is a system that automatically
detects formats such as time, and converts them to numbers for sorting.
Then, the value placeholder automatically converts them back to a similar format as the original.

This means that the `time` placeholders, which simply blindly convert the placeholder to a time format, are no longer necessary.

## How do I remove them?

Removing them is fairly simple, but might require some work if you have a lot of leaderboards that display as times.

For leaderboard placeholders, you need to is to replace `time` with `value`. For example, if you have this line of ajlb placeholders:
```
%ajlb_lb_statistic_time_played_1_alltime_name% - %ajlb_lb_statistic_time_played_1_alltime_time%
```
it should become:
```
%ajlb_lb_statistic_time_played_1_alltime_name% - %ajlb_lb_statistic_time_played_1_alltime_value%
```
Notice how the only thing that changed was the `time` at the very end.

Note that the player placeholders for showing the player's value (to themselves) is slightly different.
Instead of replacing `time` with `value,` you must remove the `_time` completely.
