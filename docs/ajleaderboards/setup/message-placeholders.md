---
id: message-placeholders
title: Message Placeholders
sidebar_label: Message Placeholders
---

import APITable from '/src/components/APITable';

Placeholders used in messages

### Signs

<APITable>

| Placeholder   | Description                                                 |
|---------------|-------------------------------------------------------------|
| `{POSITION}`  | The position on the leaderboard                             |  
| `{NAME}`      | The name of the player                                      |
| `{VALUE}`     | The score/value of the player                               |
| `{FVALUE}`    | The score/value of the player (formatted)                   |
| `{TVALUE}`    | The score/value of the player (formatted as a time)         |
| `{VALUENAME}` | The "value name" of the board (`value-names` in the config) |
| `{TIMEDTYPE}` | The TimedType of the sign (e.g. alltime, hourly, etc)       |

</APITable>