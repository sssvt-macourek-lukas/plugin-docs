---
id: placeholders
title: Placeholders
sidebar_label: Placeholders
---

import APITable from '/src/components/APITable';

These placeholders are for outputting leaderboards.
Do not try to use them in the ajLeaderboards add command!

If you don't know how to use them, check out [the example on holograms on the setup page](/ajleaderboards/setup/setup#3-optional-display-the-leaderboard-in-a-hologram).

**Do not try to use these without first [setting up the plugin](/ajleaderboards/setup/setup)!**

**__Do not forget__** to fill in `<board>` and `<number>` in the placeholders! They will not work without those.
`<board>` is the name of your leaderboard (for example, `statistic_player_kills`), and
`<number>` is the position you want to display (1 for 1st, 2 for 2nd, etc)
`<type>` is the timed type (e.g. alltime, hourly, daily, weekly, monthly)


Note that this list only applies to the latest version of ajLeaderboards. Older versions may be missing some (or all) or these placeholders.

## Leaderboard placeholders
Placeholders that are used for displaying a leaderboard

<APITable>

| Placeholder                                         | Description                                                                                                                      |
|-----------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------|
| `%ajlb_lb_<board>_<number>_<type>_name%`            | Shows the name of the player in `<number>` place on leaderboard `<board>`                                                        |
| `%ajlb_lb_<board>_<number>_<type>_displayname%`     | Shows the display name of the player in `<number>` place on leaderboard `<board>`                                                |
| `%ajlb_lb_<board>_<number>_<type>_value%`           | Shows the score of the player in `<number>` place on leaderboard `<board>`                                                       |
| `%ajlb_lb_<board>_<number>_<type>_rawvalue%`        | Shows the score of the player in `<number>` place on leaderboard `<board>`, but without commas                                   |
| `%ajlb_lb_<board>_<number>_<type>_value_formatted%` | Shows the formatted score of the player in `<number>` place on leaderboard `<board>`. Example: 10000 shows as 10k                |
| `%ajlb_lb_<board>_<number>_<type>_prefix%`          | Shows the vault prefix of the player in `<number>` place on leaderboard `<board>`                                                |
| `%ajlb_lb_<board>_<number>_<type>_suffix%`          | Shows the vault suffix of the player in `<number>` place on leaderboard `<board>`                                                |
| `%ajlb_lb_<board>_<number>_<type>_color%`           | Shows the color of the players name. It is basically the prefix but only the color codes.                                        |
| `%ajlb_lb_<board>_<number>_<type>_extra_<extra>%`   | Shows the extra placeholder for the player at that position on the leaderboard. `<extra>` is the extra placeholder (without `%`) |

</APITable>

## Relative placeholders
Placeholders that are used to show players around the player viewing the placeholder

`<relative>` is how relative to the player's position it should be. For example, `+1` there would be 1 position above. `-1` there would be one position below.

<APITable>

| Placeholder                                         | Description                                                                                                                      |
|-----------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------|
| `%ajlb_rel_<board>_<type>_<relative>_name%`            | Shows the name of the player in `<number>` place on leaderboard `<board>`                                                        |
| `%ajlb_rel_<board>_<type>_<relative>_displayname%`     | Shows the display name of the player in `<number>` place on leaderboard `<board>`                                                |
| `%ajlb_rel_<board>_<type>_<relative>_value%`           | Shows the score of the player in `<number>` place on leaderboard `<board>`                                                       |
| `%ajlb_rel_<board>_<type>_<relative>_rawvalue%`        | Shows the score of the player in `<number>` place on leaderboard `<board>`, but without commas                                   |
| `%ajlb_rel_<board>_<type>_<relative>_value_formatted%` | Shows the formatted score of the player in `<number>` place on leaderboard `<board>`. Example: 10000 shows as 10k                |
| `%ajlb_rel_<board>_<type>_<relative>_prefix%`          | Shows the vault prefix of the player in `<number>` place on leaderboard `<board>`                                                |
| `%ajlb_rel_<board>_<type>_<relative>_suffix%`          | Shows the vault suffix of the player in `<number>` place on leaderboard `<board>`                                                |
| `%ajlb_rel_<board>_<type>_<relative>_color%`           | Shows the color of the players name. It is basically the prefix but only the color codes.                                        |
| `%ajlb_rel_<board>_<type>_<relative>_extra_<extra>%`   | Shows the extra placeholder for the player at that position on the leaderboard. `<extra>` is the extra placeholder (without `%`) |

</APITable>

## Player placeholders
Placeholders that show the player that is viewing the placeholder (each player will see their own thing)

<APITable>

| Placeholder                                         | Description                                                                                                                      |
|-----------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------|
| `%ajlb_position_<board>_<type>%`                    | Shows what position the player is at in the leaderboard `<board>`                                                                |
| `%ajlb_value_<board>_<type>%`                       | Shows the user's value for `<board>`                                                                                             |
| `%ajlb_value_<board>_<type>_formatted%`             | Shows the user's value for `<board>`, and formatted                                                                              |
| `%ajlb_value_<board>_<type>_raw%`                   | Show the user's raw value for `<board>`. No formatting at all. (including commas)                                                |
| `%ajlb_extra_<extra>%`                              | Show the player's output for the extra placeholder. `<extra>` is the extra placeholder (without `%`)                             |

</APITable>

## Other placeholders
| Placeholder                                         | Description                                                                                                                      |
|-----------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------|
| `%ajlb_reset_<type>%`                               | Displays the time until the specified timed type is supposed to reset.    |
| `%ajlb_size_<board>%`                               | Shows the number of players on the specified board. Requires that the position placeholder be used somewhere.    |
