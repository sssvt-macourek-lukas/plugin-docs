---
id: luckperms-contexts
title: LuckPerms Contexts
sidebar_label: LuckPerms Contexts
---
Since ajLeaderboards v2.5.2, you can use [LuckPerms](https://luckperms.net/) contexts to give
permissions/prefixes/suffixes to players who are in certain positions in the leaderboard.

### Explanation

If you don't know about LuckPerms contexts, you can read about them [here](https://luckperms.net/wiki/Context).

ajLeaderboards adds custom (dynamic) contexts that allow you to check a player's position.

The context format is `ajlb_pos_<board>_<type>` where `<board>` is the board name, and `<type>` is the TimedType

### Example

For example, if you wanted to give permission for the essentials back command to the player who
is **#1** on the `statictic_time_played` `alltime` leaderboard, then you could use this command:
```
/luckperms group default permission set essentials.back true ajlb_pos_statistic_time_played_alltime=1
```
This will give them the permission while they are #1 on the leaderboard (and will remove the permission if they lose that spot)


