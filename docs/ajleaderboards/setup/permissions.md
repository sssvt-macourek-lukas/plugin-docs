---
id: permissions
title: Permissions
sidebar_label: Permissions
---

import APITable from '/src/components/APITable';

Here is a list of permissions for the plugin.

<APITable>

| Permission                          | Description                                                             |
|-------------------------------------|-------------------------------------------------------------------------|
| `ajleaderboards.use`                | Permission to use commands in /ajleaderboards                           |
| `ajleaderboards.dontupdate.<board>` | Any players with this permission will not be updated on the leaderboard |

</APITable>