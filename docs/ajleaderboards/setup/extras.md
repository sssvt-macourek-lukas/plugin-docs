---
id: extras
title: Extras
sidebar_label: Extras
---
Since ajLeaderboards v2.5.0, you can use extras to display things alongside players on the leaderboard, 
or to display something as a leaderboard that might not normally be able to be a leaderboard (e.g. something that's text instead of a number).

Extras don't have to be a number. They can be any kind of placeholder.

For example, you can use extras to display a player's kills and deaths alongside their kdr,
use it to display something other than a prefix/suffix next to the player's name, or use it to display custom formatting from the source plugin.

## Setup

Just like you need to add a placeholder using `/ajlb add` for ajLeaderboards to start tracking it, you need to do
something similar for extras.

Instead of doing a command to add an extra, you need to add it to the config.
Add it to the config option named `extras` (make sure to read the comment above that option)
then use `/ajlb reload` and ajLeaderboards will start tracking it.

Similar to adding a leaderboard, players who have not been online since you added the extra might not
have an extra (it would show `---`)

## Displaying alongside an existing leaderboard

For displaying an extra alongside an existing leaderboard, you should use the leaderboard extra placeholder.

Make sure to follow the setup above first.

From [the placeholders page](/ajleaderboards/setup/placeholders#%ajlb_lb_%3Cboard%3E_%3Cnumber%3E_%3Ctype%3E_extra_%3Cextra%3E%):
`%ajlb_lb_<board>_<number>_<type>_extra_<extra>%`

Simply add this placeholder to the beginning/end (wherever you want to display it) of your existing leaderboard

## Displaying custom formatting

For displaying custom formatting from an extra instead of the normal ajLeaderboards value, you should use the leaderboard extra placeholder.

You'll still have to
[setup an ajLeaderboards leaderboard with a normal number](/ajleaderboards/setup),
but when you go to display it, instead of using the `value` placeholder, you would use the extra placeholder.

From [the placeholders page](/ajleaderboards/setup/placeholders#%ajlb_lb_%3Cboard%3E_%3Cnumber%3E_%3Ctype%3E_extra_%3Cextra%3E%):
`%ajlb_lb_<board>_<number>_<type>_extra_<extra>%`

## Conclusion

If you have any questions, make sure you've read this full page first, then you can join my discord and ask me questions (invite link is on the plugin page)