---
id: main
title: Main Config
sidebar_label: Main Config
---
import ConfigFetch from '/src/components/ConfigFetch';

This is the default main config. Everything is explained in comments.


<ConfigFetch url="https://raw.githubusercontent.com/ajgeiss0702/ajLeaderboards/master/src/main/resources/config.yml"></ConfigFetch>