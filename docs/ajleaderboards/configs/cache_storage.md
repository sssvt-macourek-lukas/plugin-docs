---
id: cache_storage
title: Cache storage config
sidebar_label: Cache Storage
---
import ConfigFetch from '/src/components/ConfigFetch';

This is the default cache config. You can use it to configure where the ajLeaderboards cache is stored.

Note that h2 is the fastest storage method, therefor that's the one I recommend.

<ConfigFetch url="https://raw.githubusercontent.com/ajgeiss0702/ajLeaderboards/master/src/main/resources/cache_storage.yml"></ConfigFetch>