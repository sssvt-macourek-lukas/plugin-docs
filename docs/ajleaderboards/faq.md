---
id: faq
title: Frequently Asked Questions
sidebar_label: FAQ
---
These are a few commonly asked questions and their answers. You can use the bar on the right to skip to your question.

---

## Nobody shows up on the leaderboard (`---`)

This is probably a combination of these two FAQs:

[Admins don't show up on the leaderboard](#admins-dont-show-up-on-the-leaderboard)<br/>[Why does the leaderboard only show online players?](#why-does-the-leaderboard-only-show-online-players)

## Admins don't show up on the leaderboard!

This is most likely because they have the [`ajleaderboards.dontupdate.<board>`](setup/permissions#ajleaderboards.dontupdate.<board>) permission. This permission is given to OPs and people with the `*` permission by default.

To fix this, either negate the permission in your permission plugin, or disable the permission.

For example, to negate the permission if youre using LuckPerms, use this command:
```
/luckperms user <username> permission set ajleaderboards.dontupdate.* false
```

You can also disable the permission by disabling `enable-dontupdate-permission` in the config.

## Why does the leaderboard only show online players?
**ajLeaderboards will show offline players!**

ajLeaderboards only adds players to the leaderboard once they've joined at least once since setting up the leaderboard. That means that players will have to join once after you set up the leaderboard in order to show up on it. Once they are added to the leaderboard, they will stay on the leaderboard (even if they leave) unless you manually remove them.

You can attempt to add players manually using `/ajlb updateplayer <board> <player>`, but not all placeholders support this. If it doesn't work, you should ask the developer of the plugin that has the placeholder to add support for offline player placeholders.

## I'm trying to display the leaderboard, but it just says `Board does not exist`
Make sure you typed the board name correctly. If you are creating a sign, it should auto-complete in the command, so double check with that.
You can also check `/ajlb list` and ensure that the board is listed there.

Also make sure you did not put `%` around the board name `<board>`. For example, instead of `%statistic_player_kills%` you would put `statistic_player_kills`

If the placeholder you are trying to use does not show up in either of those, please make sure you've followed the [setup guide](/ajleaderboards/setup/setup)

## How can I show the player's prefix / rank on the leaderboard?
ajLeaderboards can display the player's prefix on the leaderboard even when they are offline.

Make sure you have [vault](https://www.spigotmc.org/resources/vault.34315/) installed on your server, then you can put the [ajLeaderboards prefix placeholder](/ajleaderboards/setup/placeholders#ajlb_lb_%3Cboard%3E_%3Cnumber%3E_%3Ctype%3E_prefix) infront of the name placeholder in order to show their prefix

## The placeholders don't work on holograms!
If you are using HolographicDisplays, I recommend switching to [DecentHolograms](https://www.spigotmc.org/resources/decent-holograms-1-8-1-19-papi-support-no-dependencies.96927/).
HolographicDisplays has made some changes that make it significantly more confusing to use placeholders for no good reason.
Because of that I would recommend moving away from it.
As mentioned earlier in this paragraph, I recommend DecentHolograms, but any hologram plugin that supports PlaceholderAPI will work (which I no longer consider HolographicDisplays to have full placeholderapi support).

This HolographicDisplays change makes no sense to me as implementing proper PlaceholderAPI support is *one line of code*

See how to convert HolographicDisplays holograms to DecentHolograms [here](https://wiki.decentholograms.eu/general/compatibility#holographicdisplays)

## My server is lagging with ajLeaderboards!

The first thing to try is to disable `blocking-fetch` in the config, then use `/ajlb reload`.

If your server is still lagging, take a timings report and send it to me on discord (invite link is on the plugin page)

## All of the positions in the leaderboard have the same value (but different players)
Make sure you read where it says ⚠️Important⚠️ in [step 1 in the setup guide](/ajleaderboards/setup/setup#1-figure-out-which-placeholder-to-use)

## How can I give rewards?

Currently, the only built-in way to give rewards is using [luckperms contexts](/ajleaderboards/setup/luckperms-contexts) to give permissions/prefixes/suffixes when a player is in a certain position on the leaderboard
