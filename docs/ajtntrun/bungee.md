---
title: Bungee mode
sidebar_label: Bungee mode
---
Bungee mode will make all players joining the server join the tntrun arena. If there are multiple arenas set up, the plugin will randomly pick one when the server starts.

To enable bungee mode, change `enable-bungee` to `true` in the config, and put the name of your lobby server in `bungee-remove-to`.
