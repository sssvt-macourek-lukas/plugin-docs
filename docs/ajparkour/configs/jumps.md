---
title: Jumps Config
sidebar_label: Jumps Config
---
import ConfigFetch from '/src/components/ConfigFetch';

This is the default jumps config. Everything is explained in comments.

<ConfigFetch url="https://gitlab.com/api/v4/projects/15546644/repository/files/src%2Fmain%2Fresources%2Fjumps.yml/raw?ref=master"></ConfigFetch>
