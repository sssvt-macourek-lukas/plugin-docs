---
id: main
title: Main Config
sidebar_label: Main Config
---
import ConfigFetch from '/src/components/ConfigFetch';

This is the default main config. Everything is explained in comments.


<ConfigFetch url="https://gitlab.com/api/v4/projects/15546644/repository/files/src%2Fmain%2Fresources%2Fconfig.yml/raw?ref=master"></ConfigFetch>