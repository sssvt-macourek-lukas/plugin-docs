The process for upgrading from v1 to v2 is fairly simple because the plugin will convert most things for you, but it leaves a lot of unused stuff behind.

The only thing it does not move over itself is the area.

# Steps
Follow these steps to upgrade and clean up the plugin.
1. Stop server
2. Take backups of ajParkour files and MySQL table (if you have one). This is not required but recommended just in case something goes wrong with the automatic conversion.
3. Delete old version jar
4. Move the v2 jar into the plugins folder
5. Start server
6. Stop server
7. Open config.yml
   - At the top of the file, it will look at the same, but at the bottom, there are several new options.
   - A lot of the options at the top of the file has been moved to a different file, or not used anymore. All of the old options except for `jump-sound` are not used, so they can be removed.
   - The options that have been moved to different files are automatically copied over to the new file.
   - It may be easier to simply delete the config.yml file and let the new config file be generated. Then there will be comments on all of them explaining what they are. Steps:
     1. Delete config.yml
     2. Start server
     3. Edit config file
     4. Reload plugin or restart server (never use /reload for anything at all, it breaks things)
8. Save config.yml
9. Start server and join
10. Re-setup area
   - Because of the new system that supports multiple parkour areas, you will need to re-set the parkour area.
   - Please see the [setup video](https://www.youtube.com/watch?v=oskllJAJBQA) or [setup guide](/ajparkour/setup/creating an area) if you do not know how to setup an area
11. Done!
    - Most of the file conversion is done by the plugin, so that is all you need to do.
