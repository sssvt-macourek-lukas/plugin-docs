import APITable from '/src/components/APITable';

Here is a list of all permissions for the plugin. All commands in the commands column are separated by commas, and they all are subcommands of /ajParkour (or it's aliases)

Please make sure you look at the correct table for the version you are using.


## v2

<APITable>

| Permission                    | Description                                                                                       | Commands              |
|-------------------------------|---------------------------------------------------------------------------------------------------|-----------------------|
| ajparkour.setup               | Allows you to use the setup commands.                                                             | setup, portals        |
| ajparkour.portals             | Allows you to setup portals                                                                       | portals               |
| ajparkour.reload              | Allows you to use /ajParkour reload                                                               | reload                |
| ajparkour.start.area          | Allows you to start parkour in a certain area                                                     | start [area]          |
| ajparkour.start.others        | Allows you to start parkour for other players                                                     | start [player]        |
| ajparkour.start.othersarea    | Allows you to start a player in a certain area                                                    | start [player] [area] |
| ajparkour.update              | Sends players with this permission update notifications and allows them to use the update command | update                |
| ajparkour.selector            | Allows you to open the block selector                                                             | blocks                |
| ajparkour.selector.openothers | Allows you to open the block selector for other players                                           | blocks [player]       |
| ajparkour.migrate             | Allows you to use /ajParkour migrate                                                              | migrate               |

</APITable>

---

## v1
| Permission| Description| Commands|
| ------ | ------ | ------ |
| ajparkour.setup | Allows you to use the setup commands. | setup, pos1, pos2, fallpos, removefallpos, portals, reset |
| ajparkour.reload| Allows you to use /ajParkour reload| reload |
| ajparkour.migrate| Allows you to use /ajParkour migrate| migrate|
| ajparkour.others| Allows you to start parkour for other players| start [player]|
| ajparkour.start| (If enabled in config) allows you to start parkour for yourself| start|
| ajpk.debug| Allows you to use /ajParkour debug| debug|
