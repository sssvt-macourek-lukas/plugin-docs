The balanced difficulty will cycle through different difficulty levels as the player gets a higher score.

| Score | Difficulty |
| ------ | ------ |
| 0-9 | EASY |
| 10-29 | MEDIUM |
| 30-69 | HARD |
| 70+ | EXPERT |